package main;

import main.domain.Destination;
import main.domain.Flight;
import main.domain.Person;
import main.domain.Reservation;
import main.flight.CollectionFlightDAO;
import main.flight.FlightController;
import main.flight.FlightService;
import main.reservation.CollectionReservationDAO;
import main.reservation.ReservationController;
import main.reservation.ReservationService;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class MainApp {

    public static Scanner console = new Scanner(System.in);

    public static void main(String[] args) throws ParseException {
        List<Flight> flights = new LinkedList<>();
        List<Reservation> reservations = new LinkedList<>();

        CollectionFlightDAO collectionFlightDao = new CollectionFlightDAO();
        collectionFlightDao.setFlightList(flights);
        FlightService flightService = new FlightService(collectionFlightDao);
        FlightController flightController = new FlightController(flightService);

        CollectionReservationDAO collectionReservationDao = new CollectionReservationDAO();
        collectionReservationDao.setReservationList(reservations);
        ReservationService reservationService = new ReservationService(collectionReservationDao);
        ReservationController reservationController = new ReservationController(reservationService);

        flights = flightController.loadFlights();
        reservations = reservationController.loadReservations();

        String menuItem = "";

        System.out.println();
        System.out.println("-=Flight Reservation App=-");

        while (!Objects.equals(menuItem, "6")) {
            System.out.println();
            System.out.println("1. Online scoreboard (all flights from Kyiv in the next 24 hours)");
            System.out.println("2. View flight information");
            System.out.println("3. Find and reserve a flight");
            System.out.println("4. Cancel reservation");
            System.out.println("5. My flights");
            System.out.println("6. Exit");
            System.out.println();
            System.out.print("Make your selection (menu item from 1 to 6): ");
            menuItem = console.nextLine();

            switch (menuItem) {

                case "1":
                    System.out.println("-=Online scoreboard (all flights from Kyiv in the next 24 hours)=-");
                    System.out.println();
                    flightController.onlineScoreboard();
                    break;

                case "2":
                    System.out.println("-=View flight information=-");
                    System.out.println();
                    System.out.print("Enter flight number please: ");
                    String flightNum = console.nextLine();
                    flightNum = flightNum.toUpperCase().trim();
                    System.out.println();
                    System.out.println(flightController.getFlightByIndex(flightNum));
                    break;

                case "3":
                    System.out.println("-=Find and book a flight=-");
                    System.out.println();
                    Destination[] destination = Destination.values();
                    for (int i = 0; i < destination.length; i++) {
                        System.out.print(i + 1);
                        System.out.print(". ");
                        System.out.println(destination[i]);
                    }
                    System.out.println();
                    System.out.print("Choose a destination from the list: ");
                    int destNum = console.nextInt() - 1;
                    console.nextLine();
                    String destString = String.valueOf(destination[destNum]);
                    System.out.print("Enter departure date (dd/MM/yyyy): ");
                    String tempFlightDate = console.nextLine();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate flightDate = LocalDate.parse(tempFlightDate, formatter);
                    System.out.print("Enter the number of passengers: ");
                    int personNum = console.nextInt();
                    console.nextLine();
                    List<Flight> flightSearch = flightController.flightSearch(destString, flightDate, personNum);
                    if (flightSearch.size() == 0) {
                        System.out.println("No flights were found for the specified parameters");
                        break;
                    }
                    System.out.println("-=The result of the search according to the specified parameters=-");
                    System.out.println();
                    flightSearch.forEach(System.out::println);
                    System.out.println();
                    System.out.print("Enter the flight number for reservation, or 0 to return to the main menu: ");
                    String resFlightNum = console.nextLine();
                    resFlightNum = resFlightNum.toUpperCase().trim();
                    if (resFlightNum.equals("0")) {
                        break;
                    }
                    Flight resFlight = flightController.getFlightByIndex(resFlightNum);
                    List<Person> people = new ArrayList<>();
                    for (int i = 0; i < personNum; i++) {
                        System.out.print("Enter the name of passenger #" + (i + 1) + ": ");
                        String personName = console.nextLine();
                        System.out.print("Enter the surname of passenger #" + (i + 1) + ": ");
                        String personSurname = console.nextLine();
                        Person passenger = new Person(personName, personSurname);
                        people.add(passenger);
                    }
                    int resNum = (reservations.size() == 0) ? 1 : reservations.get(reservations.size() - 1).getResNum() + 1;
                    Reservation reservation = new Reservation(resNum, resFlight, people);
                    reservations.add(reservation);
                    System.out.println("-=Reservation successfully completed=-");
                    System.out.println();
                    System.out.println(reservationController.getResByIndex(resNum));
                    break;

                case "4":
                    System.out.println("-=Cancel reservation=-");
                    System.out.println();
                    System.out.print("Enter the number of reservation that is being canceled: ");
                    if (!console.hasNextInt()) {
                        console.nextLine();
                        System.err.println("Invalid value, please try again");
                        continue;
                    }
                    int resCancelNum = console.nextInt();
                    console.nextLine();
                    boolean isCanceled = reservationController.resDelete(resCancelNum);
                    if (isCanceled) {
                        System.out.println("Booking #" + resCancelNum + " has been successfully cancelled");
//                        reservations.forEach(System.out::println);
                    } else {
                        System.out.println("No such reservation found");
//                        reservations.forEach(System.out::println);
                    }
                    break;

                case "5":
                    System.out.println("-=My flights=-");
                    System.out.println();
                    System.out.print("Enter your name: ");
                    String personName = console.nextLine();
                    System.out.print("Enter your surname: ");
                    String personSurname = console.nextLine();
                    System.out.println();
                    List<Reservation> myReservations = reservationController.myFlights(personName, personSurname);
                    if (myReservations.size() == 0) {
                        System.out.println("No such reservations found");
                    }
                    myReservations.forEach(System.out::println);
                    break;

                case "6":
                    System.out.println("-=Exit=-");
                    reservationController.saveReservations(reservations);
                    System.exit(0);

                default:
                    System.err.println("Invalid value, please try again");
            }
        }
    }
}