package main;

import main.domain.Destination;
import main.domain.Flight;
import main.flight.CollectionFlightDAO;
import main.flight.FlightController;
import main.flight.FlightService;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MainFlightsDatabase {
    public static void main(String[] args) {
        List<Flight> flights = new LinkedList<>();

        CollectionFlightDAO collectionFlightDao = new CollectionFlightDAO();
        FlightService flightService = new FlightService(collectionFlightDao);
        FlightController flightController = new FlightController(flightService);

        Random random = new Random();
        Destination[] destination = Destination.values();
/*
        maximum value i = 9999
*/
        for (int i = 0; i < 500; i++) {
            String flightNum = flightController.formattedFlightNum(String.valueOf(i + 1));
            long flightDate = 1674424800000L + 7200000L * i;
            int destNum = random.nextInt(5);
            int vacantSeats = random.nextInt(150);

            flights.add(new Flight(flightNum, flightDate, destination[destNum], vacantSeats));
        }
        flightController.saveFlights(flights);
    }
}