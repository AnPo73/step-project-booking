package main.domain;

public enum Destination {
    WARSAW,
    BERLIN,
    PARIS,
    LONDON,
    WASHINGTON;
}