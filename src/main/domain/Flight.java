package main.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class Flight implements Serializable {
    private String flightNum;
    private long flightDate;
    private Destination destination;
    private int vacantSeats;

    public Flight() {
    }

    public Flight(String flightNum, long flightDate, Destination destination, int vacantSeats) {
        this.flightNum = flightNum;
        this.flightDate = flightDate;
        this.destination = destination;
        this.vacantSeats = vacantSeats;
    }

    public String getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(String flightNum) {
        this.flightNum = flightNum;
    }

    public long getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(long flightDate) {
        this.flightDate = flightDate;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public int getVacantSeats() {
        return vacantSeats;
    }

    public void setVacantSeats(int vacantSeats) {
        this.vacantSeats = vacantSeats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return flightDate == flight.flightDate && vacantSeats == flight.vacantSeats && Objects.equals(flightNum, flight.flightNum) && destination == flight.destination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightNum, flightDate, destination, vacantSeats);
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightNum='" + flightNum + '\'' +
                ", flightDate=" + formattedFlightDate() +
                ", destination=" + destination +
                ", vacantSeats=" + vacantSeats +
                '}';
    }

    public String formattedFlightDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return dateFormat.format(flightDate);
    }
}