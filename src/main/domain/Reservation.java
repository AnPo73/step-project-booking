package main.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Reservation implements Serializable {
    private int resNum;
    private Flight flight;
    private List<Person> people;

    public Reservation() {
    }

    public Reservation(int resNum, Flight flight, List<Person> people) {
        this.resNum = resNum;
        this.flight = flight;
        this.people = people;
    }

    public int getResNum() {
        return resNum;
    }

    public void setResNum(int resNum) {
        this.resNum = resNum;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return resNum == that.resNum && Objects.equals(flight, that.flight) && Objects.equals(people, that.people);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resNum, flight, people);
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "resNum=" + resNum +
                ", flight=" + flight +
                ", people=" + people +
                '}';
    }
}