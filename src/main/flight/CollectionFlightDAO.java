package main.flight;

import main.domain.Flight;

import java.io.*;
import java.util.List;

public class CollectionFlightDAO implements FlightDAO {

    private List<Flight> flightList;


    @Override
    public void setFlightList(List<Flight> flightList) {

    }

    @Override
    public List<Flight> getAllFlights() {
        return flightList;
    }

    @Override
    public void saveFlights(List<Flight> flights) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("FlightsDatabase.txt"))) {
            objectOutputStream.writeObject(flights);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Flight> loadFlights() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("FlightsDatabase.txt"))) {
            flightList = (List<Flight>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return flightList;
    }

    @Override
    public Flight getFlightByIndex(String flightNum) {
        int flightID = Integer.parseInt(flightNum.substring(2)) - 1;
        if (flightID > flightList.size()) {
            return new Flight();
        } else {
            return flightList.get(flightID);
        }
    }
}