package main.flight;

import main.domain.Flight;

import java.time.LocalDate;
import java.util.List;

public class FlightController {
    private FlightService flightService;

    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    public String formattedFlightNum(String flightNum) {
        return flightService.formattedFlightNum(flightNum);
    }

    public void onlineScoreboard() {
        flightService.onlineScoreboard();
    }

    public List<Flight> getAllFlights() {
        return flightService.getAllFlights();
    }

    public void saveFlights(List<Flight> flights) {
        flightService.saveFlights(flights);
    }

    public List<Flight> loadFlights() {
        return flightService.loadFlights();
    }

    public Flight getFlightByIndex(String flightNum) {
        return flightService.getFlightByIndex(flightNum);
    }

    public List<Flight> flightSearch(String destination, LocalDate flightDate, int personNum) {
        return flightService.flightSearch(destination, flightDate, personNum);
    }
}