package main.flight;

import main.domain.Flight;

import java.util.List;

public interface FlightDAO {
    void setFlightList(List<Flight> flightList);

    List<Flight> getAllFlights();

    void saveFlights(List<Flight> flights);

    List<Flight> loadFlights();

    Flight getFlightByIndex(String flightNum);
}