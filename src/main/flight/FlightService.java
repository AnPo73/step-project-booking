package main.flight;

import main.domain.Flight;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FlightService {
    private CollectionFlightDAO flightDAO;

    public FlightService(CollectionFlightDAO flightDAO) {
        this.flightDAO = flightDAO;
    }

    String formattedFlightNum(String flightNum) {
        if (flightNum.length() < 2) {
            flightNum = "FL000" + flightNum;
        } else if (flightNum.length() < 3) {
            flightNum = "FL00" + flightNum;
        } else if (flightNum.length() < 4) {
            flightNum = "FL0" + flightNum;
        } else {
            flightNum = "FL" + flightNum;
        }
        return flightNum;
    }

    void onlineScoreboard() {
        List<Flight> flights = getAllFlights();
        List<Flight> online24 = new LinkedList<>();
        long current = System.currentTimeMillis();
        long plus24h = current + 86400000L;
        flights.forEach(f -> {
            if (f.getFlightDate() >= current && f.getFlightDate() <= plus24h) {
                online24.add(f);
            }
        });
        online24.forEach(System.out::println);
    }

    List<Flight> flightSearch(String destination, LocalDate flightDate, int personNum) {
        List<Flight> flights = getAllFlights();
        List<Flight> flightSearch = new ArrayList<>();
        flights.forEach(f -> {
            LocalDate toLocalDate = Instant.ofEpochMilli(f.getFlightDate()).atZone(ZoneId.systemDefault()).toLocalDate();
            if (f.getDestination().toString().equalsIgnoreCase(destination) &&
                    toLocalDate.getDayOfMonth() == flightDate.getDayOfMonth() &&
                    toLocalDate.getMonthValue() == flightDate.getMonthValue() &&
                    f.getVacantSeats() >= personNum) {
                flightSearch.add(f);
            }
        });
        return flightSearch;
    }

    List<Flight> getAllFlights() {
        return flightDAO.getAllFlights();
    }

    void saveFlights(List<Flight> flights) {
        flightDAO.saveFlights(flights);
    }

    public List<Flight> loadFlights() {
        return flightDAO.loadFlights();
    }

    public Flight getFlightByIndex(String flightNum) {
        return flightDAO.getFlightByIndex(flightNum);
    }
}