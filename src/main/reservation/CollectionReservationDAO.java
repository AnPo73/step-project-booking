package main.reservation;

import main.domain.Person;
import main.domain.Reservation;

import java.io.*;
import java.util.List;

public class CollectionReservationDAO implements ReservationDAO {

    private List<Reservation> reservationList;

    @Override
    public void setReservationList(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }

    @Override
    public List<Reservation> getAllReservations() {
        return reservationList;
    }

    @Override
    public void saveReservations(List<Reservation> reservations) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("ReservationsDatabase.txt"))) {
            objectOutputStream.writeObject(reservations);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Reservation> loadReservations() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("ReservationsDatabase.txt"))) {
            reservationList = (List<Reservation>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return reservationList;
    }

    @Override
    public Reservation getResByIndex(int resNum) {
        Reservation reservation = null;
        for (Reservation r : this.reservationList) {
            if (r.getResNum() == resNum) {
                reservation = r;
            }
        }
        return reservation;
    }

    @Override
    public boolean resDelete(int resCancelNum) {
        Reservation reservation = reservationList.stream().filter(r -> r.getResNum() == resCancelNum).findFirst().orElse(null);
        if (reservation == null) return false;
        reservationList.remove(reservation);
        return true;
    }

    @Override
    public List<Reservation> myFlights(String personName, String personSurname) {
        return reservationList.stream().filter(booking -> {
            List<Person> people = booking.getPeople();
            return people.stream().anyMatch(p -> (p.getName().equalsIgnoreCase(personName)
                    && p.getSurname().equalsIgnoreCase(personSurname)));
        }).toList();
    }
}