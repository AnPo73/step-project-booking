package main.reservation;

import main.domain.Reservation;

import java.util.List;

public class ReservationController {
    private ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    public List<Reservation> getAllReservations() {
        return reservationService.getAllReservations();
    }

    public void saveReservations(List<Reservation> reservations) {
        reservationService.saveReservations(reservations);
    }

    public List<Reservation> loadReservations() {
        return reservationService.loadReservations();
    }

    public Reservation getResByIndex(int resNum) {
        return reservationService.getResByIndex(resNum);
    }

    public boolean resDelete(int resCancelNum) {
        return reservationService.resDelete(resCancelNum);
    }

    public List<Reservation> myFlights(String personName, String personSurname) {
        return reservationService.myFlights(personName, personSurname);
    }
}