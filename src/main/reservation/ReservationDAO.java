package main.reservation;

import main.domain.Reservation;

import java.util.List;

public interface ReservationDAO {
    void setReservationList(List<Reservation> reservationList);

    List<Reservation> getAllReservations();

    void saveReservations(List<Reservation> reservations);

    List<Reservation> loadReservations();

    Reservation getResByIndex(int resNum);

    boolean resDelete(int resCancelNum);

    List<Reservation> myFlights(String personName, String personSurname);
}