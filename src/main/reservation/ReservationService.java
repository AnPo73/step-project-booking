package main.reservation;

import main.domain.Reservation;

import java.util.List;

public class ReservationService {
    private CollectionReservationDAO reservationDao;

    public ReservationService(CollectionReservationDAO reservationDao) {
        this.reservationDao = reservationDao;
    }

    List<Reservation> getAllReservations() {
        return reservationDao.getAllReservations();
    }

    void saveReservations(List<Reservation> reservations) {
        reservationDao.saveReservations(reservations);
    }

    List<Reservation> loadReservations() {
        return reservationDao.loadReservations();
    }

    public Reservation getResByIndex(int resNum) {
        return reservationDao.getResByIndex(resNum);
    }

    public boolean resDelete(int resCancelNum) {
        return reservationDao.resDelete(resCancelNum);
    }


    public List<Reservation> myFlights(String personName, String personSurname) {
        return reservationDao.myFlights(personName, personSurname);
    }
}